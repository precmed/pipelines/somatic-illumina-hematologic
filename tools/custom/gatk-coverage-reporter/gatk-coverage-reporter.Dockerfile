FROM r-base:latest

COPY scripts /usr/local/src/scripts
WORKDIR /usr/local/src/scripts

# Install C/C++ libraries
RUN apt-get update \
  && apt-get install -t unstable -y --no-install-recommends \
    libssl-dev \
    libxml2-dev \
    libcurl4-openssl-dev
    # libcurl4-gnutls-dev
# Install the R libraries needed to run the scripts
RUN /usr/bin/R --vanilla -f ./install_libs.R

# Execute the target script
ENTRYPOINT ["Rscript", "/usr/local/src/scripts/runCoverageReporter.R"]
CMD [""]

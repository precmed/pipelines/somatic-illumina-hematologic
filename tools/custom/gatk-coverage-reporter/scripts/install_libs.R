# source("https://bioconductor.org/biocLite.R")
# install.packages("BiocInstaller",
#                 repos="http://bioconductor.org/packages/3.6/bioc")


# install.packages('RMySQL', repos="http://cran.us.r-project.org")
# biocLite("GenomicAlignments")
# biocLite("GenomicRanges")
# biocLite("VariantAnnotation")
# biocLite("BSgenome")
# install.packages("optparse", repos="http://R-Forge.R-project.org")



# Using BiocManager
chooseCRANmirror(graphics=FALSE, ind=87)
install.packages("BiocManager")
BiocManager::install(version="3.9", ask = FALSE)
BiocManager::install(c("GenomicAlignments", "GenomicRanges", "VariantAnnotation", "BSgenome"))
install.packages("optparse", repos="http://R-Forge.R-project.org")
# BiocManager::install()
